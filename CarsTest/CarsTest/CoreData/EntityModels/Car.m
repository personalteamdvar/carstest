//
//  Car.m
//  
//
//  Created by Denis Varchenko on 8/4/16.
//
//

#import "Car.h"

@implementation Car

// Insert code here to add functionality to your managed object subclass
- (BOOL)isObjectFilled {
    return self.carDescription.length && self.model && self.price.length && self.engine && self.transmission && self.condition;
}

@end
