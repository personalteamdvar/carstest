//
//  HeaderView.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/5/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "HeaderView.h"
#import "Weather.h"

@interface HeaderView ()
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescriptioLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@end

@implementation HeaderView

- (void)setWeather:(Weather *)weather {
    _weather = weather;
    
    self.temperatureLabel.text = weather.temperature.length ? weather.temperature : @"N/A";
    self.weatherDescriptioLabel.text = weather.weatherDescription.length ? weather.weatherDescription : @"N/A";
    self.cityLabel.text = weather.cityName.length ? weather.cityName : @"N/A";
}

@end
