//
//  MainInfoTableViewCell.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "MainInfoTableViewCell.h"
#import "Constants.h"

@interface MainInfoTableViewCell () <UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) UIPickerView *pickerView;
@property (nonatomic) NSUInteger numbreOfRowsForPickerView;
@end

@implementation MainInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.numbreOfRowsForPickerView = 0;
    self.textView.delegate = self;
}

#pragma mark - IBActions
- (IBAction)textFieldEditingChanged:(UITextField *)sender {
    if (self.textFieldChanged) {
        self.textFieldChanged(sender.text);
    }
}

#pragma mark - TextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (self.textViewChanged) {
        self.textViewChanged(textView.text);
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    textView.text = @"";
}

#pragma mark - Picker view
#pragma mark Data source
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    switch (self.cellType) {
        case MainInfoTableViewCellTypeEngine:
            return [Constants titleForEngyneType:row];
        case MainInfoTableViewCellTypeTransmission:
            return [Constants titleForTransmissionType:row];
        case MainInfoTableViewCellTypeCondition:
            return [Constants titleForCondition:row];
        default:
            break;
    }
    return @"";
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.numbreOfRowsForPickerView;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

#pragma mark Delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (self.optionTextFieldChanged) {
        self.optionTextFieldChanged(row);
    }
    
    switch (self.cellType) {
        case MainInfoTableViewCellTypeEngine:
            self.textField.text = [Constants titleForEngyneType:row];
            break;
        case MainInfoTableViewCellTypeTransmission:
            self.textField.text = [Constants titleForTransmissionType:row];
            break;
        case MainInfoTableViewCellTypeCondition:
            self.textField.text = [Constants titleForCondition:row];
            break;
        default:
            self.textField.text = @"";
            break;
    }
}

#pragma mark - Setters
- (void)setCellType:(MainInfoTableViewCellType)cellType {
    _cellType = cellType;
    
    switch (cellType) {
        case MainInfoTableViewCellTypeNone:
            break;
        case MainInfoTableViewCellTypeModel:
            self.titleLabel.text = NSLocalizedString(@"addCar.carLabel", @"Car label");
            self.textField.placeholder = NSLocalizedString(@"addCar.carTextFieldPlaceHolder", @"Car field place holder");
            break;
        case MainInfoTableViewCellTypePrice:
            self.titleLabel.text = NSLocalizedString(@"addCar.carPrice", @"");
            self.textField.placeholder = NSLocalizedString(@"addCar.carPriceTextFieldPlaceHolder", @"");
            break;
        case MainInfoTableViewCellTypeEngine:
            self.titleLabel.text = NSLocalizedString(@"addCar.engineLabel", @"");
            self.textField.inputView = self.pickerView;
            self.numbreOfRowsForPickerView = 6;
            if ([self.pickerView numberOfComponents] && [self.pickerView numberOfRowsInComponent:0]) {
                [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
            }
            break;
        case MainInfoTableViewCellTypeCondition:
            self.titleLabel.text = NSLocalizedString(@"addCar.conditionLabel", @"");
            self.textField.inputView = self.pickerView;
            self.numbreOfRowsForPickerView = 4;
            if ([self.pickerView numberOfComponents] && [self.pickerView numberOfRowsInComponent:0]) {
                [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
            }
            break;
        case MainInfoTableViewCellTypeTransmission:
            self.titleLabel.text = NSLocalizedString(@"addCar.transmissionLabel", @"");
            self.textField.inputView = self.pickerView;
            self.numbreOfRowsForPickerView = 4;
            if ([self.pickerView numberOfComponents] && [self.pickerView numberOfRowsInComponent:0]) {
                [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
            }
            break;
        case MainInfoTableViewCellTypeDescription:
            self.titleLabel.text = NSLocalizedString(@"addCar.descriptionLabel", @"");
            self.textView.text = NSLocalizedString(@"addCar.descriptionTextViewPlaceHolder", @"");
            break;
    }
}

#pragma mark - Getters
- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [UIPickerView new];
        _pickerView.delegate = self;
    }
    return _pickerView;
}

@end
