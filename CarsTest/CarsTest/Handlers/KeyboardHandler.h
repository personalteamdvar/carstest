//
//  KeyboardHandler.h
//  GotToTrain
//
//  Created by Denis Varchenko on 9/29/15.
//  Copyright © 2015 Migon Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardHandler : NSObject

@property (weak, nonatomic) UIView *view;
@property (weak, nonatomic) UIScrollView *scrollView;

@property (weak, nonatomic) UITextField *sender;
@property (assign, nonatomic) CGPoint centerPosition;
@property (assign, nonatomic) NSInteger offset;

@property (assign, nonatomic) BOOL usesKeyboardHanlerScroll;

-(instancetype)initWithScrollView:(UIScrollView *)scrollView;

-(void)addTapGestureRecognizer;
-(void)removeTapGestureRecognizer;

@end
