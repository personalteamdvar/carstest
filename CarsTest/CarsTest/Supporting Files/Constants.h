//
//  Constants.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, EngineType) {
    EngineTypeUnlisted,
    EngineTypeBiFuel,
    EngineTypeDiesel,
    EngineTypeElectric,
    EngineTypeHybrid,
    EngineTypeLpgPetrol
};

typedef NS_ENUM(NSUInteger, TransmissionType) {
    TransmissionTypeUnlisted,
    TransmissionTypeAutomatic,
    TransmissionTypeManual,
    TransmissionTypeSemiAutomatic
};

typedef NS_ENUM(NSUInteger, Condition) {
    ConditionVeryGood,
    ConditionGood,
    ConditionNormal,
    ConditionBad
};

@interface Constants : NSObject

+ (NSString *)titleForEngyneType:(EngineType)engineType;
+ (NSString *)titleForTransmissionType:(TransmissionType)transmissionType;
+ (NSString *)titleForCondition:(Condition)condition;

@end
