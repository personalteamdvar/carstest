//
//  AddImagesTableViewCell.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo, AddImagesTableViewCell;

@protocol AddImagesTableViewCellDelegate <NSObject>

- (UIImage *)tableViewCell:(AddImagesTableViewCell *)tableViewCell imageForIndex:(NSUInteger)index;
- (NSUInteger)numberOfImages:(AddImagesTableViewCell *)cell;

@end

@interface AddImagesTableViewCell : UITableViewCell
@property (weak, nonatomic) id<AddImagesTableViewCellDelegate> delegate;

@property (copy, nonatomic) void (^addImageCellPressed) (AddImagesTableViewCell *cell);
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end
