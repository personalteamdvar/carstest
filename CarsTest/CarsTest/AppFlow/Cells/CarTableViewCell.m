//
//  CarTableViewCell.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "CarTableViewCell.h"

#import "FileManager.h"

#import "Car.h"

@interface CarTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *carImageView;
@property (weak, nonatomic) IBOutlet UILabel *carNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carPriceLAbel;
@end

@implementation CarTableViewCell

- (void)setCar:(Car *)car {
    _car = car;
    
    self.carNameLabel.text = car.model;
    self.carPriceLAbel.text = car.price;
    
    [FileManager getImageForFileName:[self.car.photos array].firstObject.path withCompletion:^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.carImageView.image = image;
        });
    }];
}

@end
