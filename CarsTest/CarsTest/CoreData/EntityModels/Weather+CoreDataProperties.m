//
//  Weather+CoreDataProperties.m
//  
//
//  Created by Denis Varchenko on 8/4/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Weather+CoreDataProperties.h"

@implementation Weather (CoreDataProperties)

@dynamic cityName;
@dynamic temperature;
@dynamic weatherDescription;
@dynamic icon;

@end
