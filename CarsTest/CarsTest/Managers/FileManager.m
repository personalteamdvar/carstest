//
//  FileManager.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+ (BOOL)saveImage:(UIImage *)image withName:(NSString *)fileName {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData writeToFile:filePath atomically:YES];
}

+ (void)getImageForFileName:(NSString *)fileName withCompletion:(void (^)(UIImage *))completion {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSData *imageData = UIImageJPEGRepresentation([UIImage imageWithContentsOfFile:filePath], 0.1f);
        completion([UIImage imageWithData:imageData]);
    });
}

+ (BOOL)removeImageWithName:(NSString *)fileName {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    
    return [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
}

@end
