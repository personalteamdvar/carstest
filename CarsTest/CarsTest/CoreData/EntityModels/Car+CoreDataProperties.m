//
//  Car+CoreDataProperties.m
//  
//
//  Created by Denis Varchenko on 8/4/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Car+CoreDataProperties.h"

@implementation Car (CoreDataProperties)

@dynamic model;
@dynamic price;
@dynamic engine;
@dynamic transmission;
@dynamic condition;
@dynamic carDescription;
@dynamic photos;

@end
