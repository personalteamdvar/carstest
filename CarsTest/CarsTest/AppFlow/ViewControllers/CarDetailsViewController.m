//
//  CarDetailsViewController.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "CarDetailsViewController.h"

#import "MainInfoTableViewCell.h"
#import "MainDetailsTableViewCell.h"
#import "ImagesTableViewCell.h"

#import <MagicalRecord/MagicalRecord.h>
#import "Car.h"

#import "Constants.h"

static NSString *const kImagesCell = @"imagesCell";
static NSString *const kMainDetailsCell = @"mainDetailsCell";
static NSString *const kDetailsCell = @"detailsCell";
static NSString *const kDescriptionCell = @"descriptionCell";

@interface CarDetailsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *carDetailTableView;
@end

@implementation CarDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.carDetailTableView.estimatedRowHeight = 60.f;
    self.carDetailTableView.rowHeight = UITableViewAutomaticDimension;
    self.carDetailTableView.tableFooterView = [UIView new];
}

#pragma mark - Table view
#pragma mark Data source
//I know it looks horible but I'm too tired to find better way. Shame on me
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            ImagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kImagesCell];
            cell.photosArray = [self.car.photos array];
            return cell;
        }
        case 1: {
            MainDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMainDetailsCell];
            cell.priceLabel.text = self.car.price;
            cell.modelLabel.text = self.car.model;
            return cell;
        }
        case 2: {
            MainInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailsCell];
            cell.cellType = MainInfoTableViewCellTypeEngine;
            cell.textField.text = [Constants titleForEngyneType:[self.car.engine integerValue]];
            [cell setUserInteractionEnabled:NO];
            return cell;
        }
        case 3: {
            MainInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailsCell];
            cell.cellType = MainInfoTableViewCellTypeTransmission;
            cell.textField.text = [Constants titleForTransmissionType:[self.car.transmission integerValue]];
            [cell setUserInteractionEnabled:NO];
            return cell;
        }
        case 4: {
            MainInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailsCell];
            cell.cellType = MainInfoTableViewCellTypeCondition;
            cell.textField.text = [Constants titleForCondition:[self.car.condition integerValue]];
            [cell setUserInteractionEnabled:NO];
            return cell;
        }
        case 5: {
            MainInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDescriptionCell];
            cell.cellType = MainInfoTableViewCellTypeDescription;
            cell.textView.text = self.car.carDescription;
            [cell setUserInteractionEnabled:NO];
            return cell;
        }
        default:
            break;
    }
    return [UITableViewCell new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

@end
