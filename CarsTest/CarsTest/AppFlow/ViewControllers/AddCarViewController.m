//
//  AddCarViewController.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "AddCarViewController.h"

#import "AddImagesTableViewCell.h"
#import "MainInfoTableViewCell.h"

#import <MagicalRecord/MagicalRecord.h>
#import "Car.h"

#import "UIColor+HexString.h"

#import "KeyboardHandler.h"

#import "FileManager.h"

static NSString *const kImagesCell = @"imagesCell";
static NSString *const kMainInfoCell = @"mainInfoCell";
static NSString *const kDetailsCell = @"detailsCell";
static NSString *const kDescriptionCell = @"descriptionCell";

@interface AddCarViewController () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, AddImagesTableViewCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *detailsTableView;
@property (strong, nonnull) Car *car;
@property (nonatomic, strong, nonnull) NSMutableArray<UIImage *> *images;

@property (strong, nonatomic) KeyboardHandler *keyboardHandler;
@end

@implementation AddCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.detailsTableView.estimatedRowHeight = 60.f;
    self.detailsTableView.rowHeight = UITableViewAutomaticDimension;
    self.detailsTableView.tableFooterView = [UIView new];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self.navigationController.interactivePopGestureRecognizer addTarget:self
                                                                  action:@selector(handlePopGestureRecognizer:)];
    
    self.car = [Car MR_createEntity];
    
    self.keyboardHandler = [[KeyboardHandler alloc] initWithScrollView:self.detailsTableView];
}

#pragma mark - IBActions
- (IBAction)addBarButtonPressed:(id)sender {
    if (![self validateFields]) {
        return;
    }
    
    for (UIImage *image in self.images) {
        NSString *fileName = [NSString stringWithFormat:@"%@.png", [NSDate date].description];
        if ([FileManager saveImage:image withName:fileName]) {
            Photo *photo = [Photo MR_createEntity];
            photo.path = fileName;
            [self.car addPhotosObject:photo];
        }
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (contextDidSave) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.car MR_deleteEntity];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)handlePopGestureRecognizer:(UIGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        [self.car MR_deleteEntity];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Actions
- (BOOL)validateFields {
    if (![self.car isObjectFilled]) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"alerterror.fieldarenotfielderror", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"alertaction.ok", nil)
                                                       style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:controller animated:YES completion:nil];
    }
    return [self.car isObjectFilled];
}

#pragma mark - Table view
#pragma mark Data source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) __weakSelf = self;
    
    if (indexPath.row == 0) {
        AddImagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kImagesCell];
        cell.delegate = self;
        [cell.collectionView reloadData];
        cell.addImageCellPressed = ^(AddImagesTableViewCell *cell) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            [imagePickerController.navigationBar setTranslucent:NO];
            [imagePickerController.navigationBar setBarTintColor:self.view.backgroundColor];
            [imagePickerController.navigationBar setTintColor:[UIColor whiteColor]];
            [imagePickerController.navigationBar setTitleTextAttributes:@{
                                                                          NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                          }];
            
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagePickerController.delegate = __weakSelf;
            [__weakSelf presentViewController:imagePickerController animated:YES completion:nil];
        };
        return cell;
    } else {
        MainInfoTableViewCell *cell;
        switch (indexPath.row) {
            case 1: {
                cell = [tableView dequeueReusableCellWithIdentifier:kMainInfoCell];
                cell.textFieldChanged = ^(NSString *text) {
                    __weakSelf.car.model = text;
                };
                break;
            }
            case 2: {
                cell = [tableView dequeueReusableCellWithIdentifier:kMainInfoCell];
                cell.textFieldChanged = ^(NSString *text) {
                    __weakSelf.car.price = text;
                };
                break;
            }
            case 3: {
                cell = [tableView dequeueReusableCellWithIdentifier:kDetailsCell];
                cell.optionTextFieldChanged = ^(NSUInteger optionID) {
                    __weakSelf.car.engine = @(optionID);
                };
                break;
            }
            case 4: {
                cell = [tableView dequeueReusableCellWithIdentifier:kDetailsCell];
                cell.optionTextFieldChanged = ^(NSUInteger optionID) {
                    __weakSelf.car.transmission = @(optionID);
                };
                break;
            }
            case 5: {
                cell = [tableView dequeueReusableCellWithIdentifier:kDetailsCell];
                cell.optionTextFieldChanged = ^(NSUInteger optionID) {
                    __weakSelf.car.condition = @(optionID);
                };
                break;
            }
            case 6: {
                cell = [tableView dequeueReusableCellWithIdentifier:kDescriptionCell];
                cell.textViewChanged = ^(NSString *text) {
                    __weakSelf.car.carDescription = text;
                    CGPoint offset = __weakSelf.detailsTableView.contentOffset;
                    [UIView setAnimationsEnabled:NO];
                    [__weakSelf.detailsTableView beginUpdates];
                    [__weakSelf.detailsTableView endUpdates];
                    [UIView setAnimationsEnabled:YES];
                    [__weakSelf.detailsTableView setContentOffset:offset];
                };
                break;
            }
            default:
                break;
        }
        
        cell.cellType = indexPath.row;
        return cell;
    }
       return [UITableViewCell new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

#pragma mark Delegate

#pragma mark - Add images table view cell delegate
- (UIImage *)tableViewCell:(AddImagesTableViewCell *)tableViewCell imageForIndex:(NSUInteger)index {
    return self.images[index];
}

- (NSUInteger)numberOfImages:(AddImagesTableViewCell *)cell {
    return self.images.count;
}

#pragma mark - Image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [self.images addObject:image];
    [self.detailsTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]
                                 withRowAnimation:UITableViewRowAnimationNone];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Getter
- (NSArray<UIImage *> *)images {
    if (!_images) {
        _images = [NSMutableArray new];
    }
    return _images;
}

@end
