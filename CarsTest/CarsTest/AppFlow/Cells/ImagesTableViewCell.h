//
//  ImagesTableViewCell.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/5/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo;

@interface ImagesTableViewCell : UITableViewCell
@property (strong, nonatomic) NSArray<Photo *> *photosArray;
@end
