//
//  ImageCollectionViewCell.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (void)prepareForReuse {
    self.carImageView.image = nil;
}

@end
