//
//  FileManager.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FileManager : NSObject

+ (BOOL)saveImage:(UIImage *)image withName:(NSString *)fileName;
+ (BOOL)removeImageWithName:(NSString *)fileName;
+ (void)getImageForFileName:(NSString *)fileName withCompletion:(void (^)(UIImage *image))completion;

@end
