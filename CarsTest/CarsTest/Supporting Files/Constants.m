//
//  Constants.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "Constants.h"

@implementation Constants

+ (NSString *)titleForCondition:(Condition)condition {
    switch (condition) {
        case ConditionVeryGood:
            return NSLocalizedString(@"condition.verygood", @"Very good condition");
        case ConditionGood:
            return NSLocalizedString(@"condition.good", @"Good condition");
        case ConditionNormal:
            return NSLocalizedString(@"condition.normal", @"Normal condition");
        case ConditionBad:
            return NSLocalizedString(@"condition.bad", @"Very bad condition");
        default:
            break;
    }
    return NSLocalizedString(@"unknown", @"Unknown");
}

+ (NSString *)titleForEngyneType:(EngineType)engineType {
    switch (engineType) {
        case EngineTypeUnlisted:
            return NSLocalizedString(@"engine.unlisted", @"Engine type not in list");
        case EngineTypeBiFuel:
            return NSLocalizedString(@"engine.bifuel", @"BiFuel Engine");
        case EngineTypeDiesel:
            return NSLocalizedString(@"engine.diesel", @"Diesel engine");
        case EngineTypeElectric:
            return NSLocalizedString(@"engine.electric", @"Electric engine");
        case EngineTypeHybrid:
            return NSLocalizedString(@"engine.hybrid", @"Hybrid engine");
        case EngineTypeLpgPetrol:
            return NSLocalizedString(@"engine.petrol", @"Petrol engine");
        default:
            break;
    }
    return NSLocalizedString(@"unknown", @"Unknown");
}

+ (NSString *)titleForTransmissionType:(TransmissionType)transmissionType {
    switch (transmissionType) {
        case TransmissionTypeUnlisted:
            return NSLocalizedString(@"transmission.unlisted", @"Transmission type not i lsit");
        case TransmissionTypeAutomatic:
            return NSLocalizedString(@"transmission.automatic", @"Automatic transmisison");
        case TransmissionTypeManual:
            return NSLocalizedString(@"transmission.manual", @"Manual transmission");
        case TransmissionTypeSemiAutomatic:
            return NSLocalizedString(@"transmission.semiautomatic", @"Semiautomatic transmission");
        default:
            break;
    }
    return NSLocalizedString(@"unknown", @"Unknown");
}

@end
