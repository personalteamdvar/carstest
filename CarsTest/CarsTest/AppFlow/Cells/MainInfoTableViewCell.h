//
//  MainInfoTableViewCell.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MainInfoTableViewCellType) {
    MainInfoTableViewCellTypeNone,
    MainInfoTableViewCellTypeModel,
    MainInfoTableViewCellTypePrice,
    MainInfoTableViewCellTypeEngine,
    MainInfoTableViewCellTypeTransmission,
    MainInfoTableViewCellTypeCondition,
    MainInfoTableViewCellTypeDescription
};

@interface MainInfoTableViewCell : UITableViewCell
@property (nonatomic) MainInfoTableViewCellType cellType;

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (copy, nonatomic) void (^textFieldChanged) (NSString *text);
@property (copy, nonatomic) void (^optionTextFieldChanged) (NSUInteger optionID);
@property (copy, nonatomic) void (^textViewChanged) (NSString *text);

@end
