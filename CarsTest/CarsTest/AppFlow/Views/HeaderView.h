//
//  HeaderView.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/5/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Weather;

@interface HeaderView : UIView
@property (weak, nonatomic) Weather *weather;
@end
