//
//  Photo+CoreDataProperties.h
//  
//
//  Created by Denis Varchenko on 8/4/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Photo.h"

NS_ASSUME_NONNULL_BEGIN

@interface Photo (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *path;

@end

NS_ASSUME_NONNULL_END
