//
//  AddImagesTableViewCell.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "AddImagesTableViewCell.h"

#import "ImageCollectionViewCell.h"

#import "FileManager.h"

#import "Photo.h"

static NSString *const kImageCellIdentifier = @"imageCell";
static NSString *const kAddImageCellIdentifier = @"addImageCell";

@interface AddImagesTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegate>

@end

@implementation AddImagesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

#pragma mark - Collection view
#pragma mark Data Source
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [collectionView numberOfItemsInSection:indexPath.section] - 1) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAddImageCellIdentifier forIndexPath:indexPath];
        return cell;
    } else {
        ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kImageCellIdentifier forIndexPath:indexPath];
        
        if ([self.delegate respondsToSelector:@selector(tableViewCell:imageForIndex:)]) {
            cell.carImageView.image = [self.delegate tableViewCell:self imageForIndex:indexPath.row];
        } else {
            cell.carImageView.image = nil;
        }
        return cell;
    }
    return [UICollectionViewCell new];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSUInteger numberOfImages = 0;
    if ([self.delegate respondsToSelector:@selector(numberOfImages:)]) {
        numberOfImages = [self.delegate numberOfImages:self];
    }
    return numberOfImages + 1;
}

#pragma mark Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == [collectionView numberOfItemsInSection:indexPath.section] - 1 && self.addImageCellPressed) {
        self.addImageCellPressed(self);
    }
}

@end
