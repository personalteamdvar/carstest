//
//  ImagesTableViewCell.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/5/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "ImagesTableViewCell.h"

#import "ImageCollectionViewCell.h"

#import "FileManager.h"

#import "Photo.h"

static NSString *const kImageCellIdentifier = @"imageCell";

@interface ImagesTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) NSMutableDictionary *images;
@end

@implementation ImagesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

#pragma mark - Collection view
#pragma mark Data Source
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kImageCellIdentifier forIndexPath:indexPath];
    UIImage *image = self.images[self.photosArray[indexPath.row].path];
    if (image) {
        cell.carImageView.image = image;
    } else {
        [FileManager getImageForFileName:self.photosArray[indexPath.row].path withCompletion:^(UIImage *image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.carImageView.image = image;
                self.images[self.photosArray[indexPath.row].path] = image;
            });
        }];
    }
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photosArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return collectionView.bounds.size;
}

#pragma mark ScrollView
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[self convertPoint:scrollView.center toView:scrollView]];
    self.pageControl.currentPage = indexPath.row;
}

#pragma mark - Setters
- (void)setPhotosArray:(NSArray<Photo *> *)photosArray {
    _photosArray = photosArray;
    
    self.pageControl.numberOfPages = photosArray.count;
}

#pragma mark - Getters
- (NSMutableDictionary *)images {
    if (!_images) {
        _images = [NSMutableDictionary new];
    }
    return _images;
}

@end
