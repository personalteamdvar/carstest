//
//  CarsListViewController.m
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import "CarsListViewController.h"
#import "CarDetailsViewController.h"

#import "CarTableViewCell.h"

#import "HeaderView.h"

#import "FileManager.h"

#import <MagicalRecord/MagicalRecord.h>
#import "Car.h"
#import "Weather.h"

#import <CoreLocation/CoreLocation.h>

static NSString *const kCarsTableViewCellIdentifier = @"carCell";

static NSString *const kCarDetailsSegue = @"carDetailsSegue";

@interface CarsListViewController () <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *carsTableView;
@property (weak, nonatomic) IBOutlet HeaderView *headerView;

@property (strong, nonatomic) NSURLSessionDataTask *dataTask; //is used to prevent multiply request in time

@property (strong, nonnull) NSArray *carsDataSource;

@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation CarsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.carsTableView.rowHeight = 70.f;
    self.carsTableView.tableFooterView = [UIView new];
    
    [self initLocationManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.carsDataSource = [Car MR_findAll];
    [self.carsTableView reloadData];
    
    [self updateHeaderWithWeather:[Weather MR_findFirst]];
    
    [self.locationManager startUpdatingLocation];
}

#pragma mark - Actions
- (void)updateHeaderWithWeather:(Weather *)weather {
    [self.headerView setWeather:weather];
}

- (void)initLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
}

- (void)loadWheatherForLocation:(CLLocation *)location completion:(void(^)(NSDictionary *data, NSError *error))completion {
    //Prevent performing new request if previous is running
    if (self.dataTask.state == NSURLSessionTaskStateRunning) {
        return;
    }
    
    //Of course I'm ususaly using webservice for these puposes to minimize code
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&APPID=52859522a831156ff27d52abac2d24a7", location.coordinate.latitude, location.coordinate.longitude];
    NSLog(@"Sent url: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    self.dataTask = [session dataTaskWithURL:url
                           completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                               NSDictionary *json;
                               if (data) {
                                   json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                               }
                               completion(json, error);
                           }];
    [self.dataTask resume];
}

#pragma mark - Location manager delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self loadWheatherForLocation:locations.lastObject completion:^(NSDictionary *data, NSError *error) {
        if (error) {
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [controller addAction:action];
            [self presentViewController:controller animated:YES completion:nil];
        } else {
            NSLog(@"%@", data);
            if ([data[@"cod"] integerValue] == 200) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Weather MR_truncateAll];
                    
                    Weather *weather = [Weather MR_createEntity];
                    //All fields may come as [nsnull null] so usually I need to check for it
                    weather.cityName = data[@"name"];
                    weather.temperature = [data[@"main"][@"temp"] stringValue];
                    
                    NSDictionary *weatherDict = [data[@"weather"] firstObject];
                    weather.weatherDescription = weatherDict[@"description"];
                    weather.icon = weatherDict[@"icon"];
                    
                    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
                        [self updateHeaderWithWeather:weather];
                        if (contextDidSave) {
                            NSLog(@"Weather saved succesfully");
                        } else {
                            NSLog(@"Error: %@", [error localizedDescription]);
                        }
                    }];
                });
            }
        }

    }];
    [manager stopUpdatingLocation];
}

#pragma mark - Table view
#pragma mark Data source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCarsTableViewCellIdentifier];
    cell.car = self.carsDataSource[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.carsDataSource.count;
}

#pragma marl Delegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Car *carToRemove = self.carsDataSource[indexPath.row];
        
        for (Photo *photo in carToRemove.photos) {
            [FileManager removeImageWithName:photo.path];
        }
        
        [carToRemove MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
        self.carsDataSource = [Car MR_findAll];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kCarDetailsSegue]) {
        NSIndexPath *indexPath = [self.carsTableView indexPathForSelectedRow];
        Car *car = self.carsDataSource[indexPath.row];
        
        CarDetailsViewController *dvc = segue.destinationViewController;
        dvc.car = car;
    }
}

@end
