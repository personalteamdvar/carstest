//
//  MainDetailsTableViewCell.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;

@end
