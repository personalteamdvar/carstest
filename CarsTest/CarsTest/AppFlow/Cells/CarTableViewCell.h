//
//  CarTableViewCell.h
//  CarsTest
//
//  Created by Denis Varchenko on 8/4/16.
//  Copyright © 2016 Denis Varchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Car;

@interface CarTableViewCell : UITableViewCell
@property (strong, nonatomic) Car *car;
@end
