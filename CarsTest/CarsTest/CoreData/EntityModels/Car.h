//
//  Car.h
//  
//
//  Created by Denis Varchenko on 8/4/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Car : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
- (BOOL)isObjectFilled;

@end

NS_ASSUME_NONNULL_END

#import "Car+CoreDataProperties.h"
