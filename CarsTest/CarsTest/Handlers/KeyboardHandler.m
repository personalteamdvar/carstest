//
//  KeyboardHandler.m
//  GotToTrain
//
//  Created by Denis Varchenko on 9/29/15.
//  Copyright © 2015 Migon Software. All rights reserved.
//

#import "KeyboardHandler.h"

@interface KeyboardHandler()

@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (assign, nonatomic) BOOL keyboardUp;

@end

@implementation KeyboardHandler {
    CGFloat bottomScrollViewInset;
    CGFloat topScrollViewInsets;
}

- (instancetype)initWithScrollView:(UIScrollView *)scrollView {
    self = [super init];
    if (self) {
        self.usesKeyboardHanlerScroll = YES;
        [self registerForKeyboardNotifications];
        
        _scrollView = scrollView;
        bottomScrollViewInset = self.scrollView.contentInset.bottom;
        topScrollViewInsets = self.scrollView.contentInset.top;
        
        self.offset = 0;
    }
    return self;
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    if (self.scrollView) {
//        UIEdgeInsets insets = self.scrollView.contentInset;
//        insets.bottom = bottomScrollViewInset;
//        [self.scrollView setContentInset:insets];
        
        [self.scrollView setContentInset:UIEdgeInsetsMake(topScrollViewInsets, 0, bottomScrollViewInset, 0)];

        [self.scrollView setContentOffset:CGPointMake(0, -topScrollViewInsets) animated:YES];
        self.keyboardUp = NO;
    }
}

- (void)keyboardWillBeShown:(NSNotification *)notification {
// This one is called every time in iOS 9 (maybe higher also) when text field is tapped and is called once in iOS 8.4
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(topScrollViewInsets, 0, keyboardEndFrame.size.height, 0)];

    self.keyboardUp = YES;
    
    if(!self.usesKeyboardHanlerScroll)
        return;
    
    if (self.scrollView) {
        if(self.sender) {
            CGFloat yOffset = self.centerPosition.y - self.sender.frame.origin.y;
            yOffset = fabsf((float)yOffset);
            
//            [self.scrollView setContentInset:UIEdgeInsetsMake(topScrollViewInsets, 0, keyboardEndFrame.size.height + fabs(yOffset - keyboardEndFrame.size.height), 0)];

            [self.scrollView setContentOffset:CGPointMake(0, yOffset) animated:YES];
        }

//        UIEdgeInsets insets = self.scrollView.contentInset;
////        bottomScrollViewInset = insets.bottom;
//        insets.bottom = keyboardEndFrame.size.height;
//        [self.scrollView setContentInset:insets];
    }
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [self removeTapGestureRecognizer];
}

#pragma mark - Setters

-(void)setSender:(UITextField *)sender {
    _sender = sender;
    
    if(!self.keyboardUp)
        return;
    
    if(self.scrollView && _sender) {
        CGFloat yOffset = self.centerPosition.y - self.sender.frame.origin.y;
        yOffset = fabsf((float)yOffset);
        
        [self.scrollView setContentOffset:CGPointMake(0, yOffset) animated:YES];
    }
}


#pragma mark - Gesture reconzier methods

-(void)addTapGestureRecognizer {
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    if(self.view) {
        [self.view addGestureRecognizer:self.tapRecognizer];
    }
}

-(void)removeTapGestureRecognizer {
    if(self.tapRecognizer) {
        [self.view removeGestureRecognizer:self.tapRecognizer];
        self.tapRecognizer = nil;
    }
}

-(void)didTap:(UITapGestureRecognizer*)tapRecognizer {
    [self.view endEditing:YES];
}

@end
